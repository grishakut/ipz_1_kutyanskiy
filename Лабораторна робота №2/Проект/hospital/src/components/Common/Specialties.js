export const specialties = [
  "Allergists",
  "Anesthesiologists",
  "Cardiologists",
  "Dermatologists",
  "Endocrinologists",
  "Family Physicians",
  "Gastroenterologists",
  "Geriatric Specialists",
  "Hematologists",
  "Palliative Specialists",
  "Infectious Disease Specialists",
  "Internists",
  "Medical Geneticists",
  "Nephrologists",
  "Neurologists",
  "Obstetricians and Gynecologists",
  "Oncologists",
  "Ophthalmologists",
  "Osteopaths",
  "Otolaryngologists",
  "Pathologists",
  "Pediatricians",
  "Physiatrists",
  "Plastic Surgeons",
  "Podiatrists",
  "Preventive Specialists",
  "Psychiatrists",
  "Pulmonologists",
  "Radiologists",
  "Rheumatologists",
  "Sleep Specialists",
  "Sports Specialists",
  "General Surgeons",
  "Urologists",
];

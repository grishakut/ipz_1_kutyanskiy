const { Router } = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");

const Patient = require("../models/Patient");
const Doctor = require("../models/Doctor");
const Request = require("../models/Request");
const Examination = require("../models/Examination");

const auth = require("../middleware/auth.middleware.js");

const { validateEmail, validateName, validatePassword, validateSurname } = require("../utils/validatiors");

const router = Router();

// api/doctor/register
router.post("/register", async (req, res) => {
  try {
    const { name, surname, email, specialtyType, password, repeatPassword } = req.body;

    const doctorToRegister = await Doctor.findOne({ email });
    if (doctorToRegister) {
      console.error("ERROR: USER ALREADY EXIST");
      return res.status(400).json({ message: "User already exist" });
    }

    if (password !== repeatPassword) {
      console.error("ERROR: PASSWORDS AREN'T EQUAL");
      return res.status(400).json({ message: "Passwords aren't equal" });
    }

    let validatedName = validateName(name);
    if(!validatedName.OK){
      return res.status(400).json({ message: validatedName.message });
    }

    let validatedSurname = validateSurname(surname);
    if(!validatedSurname.OK){
      return res.status(400).json({ message: validatedSurname.message });
    }

    let validatedEmail = validateEmail(email);
    if(!validatedEmail.OK){
      return res.status(400).json({ message: validatedEmail.message });
    }

    let validatedPassword = validatePassword(password);
    if(!validatedPassword.OK){
      return res.status(400).json({ message: validatedPassword.message });
    }

    if(specialtyType < 0){
      return res.status(400).json({ message: "Specialty is required" });
    }

    const hashedPassword = await bcrypt.hash(password, 15);
    const user = new Doctor({ name, surname, email, specialtyType, password: hashedPassword });

    await user.save();

    const createdUser = await Doctor.findOne({ name, surname, email, specialtyType });

    const token = jwt.sign(
      {
        userId: user.id,
      },
      config.get("jwtSecret"),
      { expiresIn: "30d" }
    );

    res.json({ token, userId: createdUser.id, name, surname, email, specialtyType });
  } catch (e) {
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/doctor/login
router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;

    console.log("email",email);
    console.log("password",password);

    const user = await Doctor.findOne({ email });

    let isMatch;
    if (user) {
      isMatch = await bcrypt.compare(password, user.password);
    }

    if (!user || !isMatch) {
      return res.status(404).json({ message: "Such user doesn't exist" });
    }

    const token = jwt.sign(
      {
        userId: user.id,
      },
      config.get("jwtSecret"),
      { expiresIn: "30d" }
    );

    res.json({ 
      token,
      userId: user.id,
      name: user.name,
      surname: user.surname,
      email: user.email,
      specialtyType: user.specialtyType 
    });
  } catch (e) {
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/doctor/profile/update
router.post("/profile/update", auth, async (req, res) => {
  try {
    const { id, name, surname, email, password, specialtyType } = req.body;

      let fieldsToUpdate = {
        name, surname, email, password, specialtyType
      }
  
      if(!name){
        delete fieldsToUpdate.name;
      }
  
      if(!surname){
        delete fieldsToUpdate.surname;
      }
  
      if(!email){
        delete fieldsToUpdate.email;
      }
  
      if(!password){
        delete fieldsToUpdate.password;
      }

      if(specialtyType < 0 || !specialtyType){
        delete fieldsToUpdate.specialtyType;
      }

      if(email){
        const emailToUpdate = await Doctor.findOne({ email });
        if (emailToUpdate) {
          console.error("ERROR: USER ALREADY EXIST");
          return res.status(400).json({ message: "Email is in use" });
        }
        let validatedEmail = validateEmail(email);
        if(!validatedEmail.OK){
          return res.status(400).json({ message: validatedEmail.message });
        }
      }

      if(name){
        let validatedName = validateName(name);
        if(!validatedName.OK){
          return res.status(400).json({ message: validatedName.message });
        }
      }

      if(surname){
        let validatedSurname = validateSurname(surname);
        if(!validatedSurname.OK){
          return res.status(400).json({ message: validatedSurname.message });
        }
      }

      let hashedPassword;

      if(password){
        let validatedPassword = validatePassword(password);
        if(!validatedPassword.OK){
          return res.status(400).json({ message: validatedPassword.message });
        }

        hashedPassword = await bcrypt.hash(password, 15);
      }

      await Doctor.update(
        {_id: id}, 
        {$set: {...fieldsToUpdate, password: hashedPassword}}, 
        (e) => console.log(e));

    const updatedUser = await Doctor.findOne({ _id: id });

    res.status(201).json(updatedUser);
  } catch (e) {
    console.error(e);
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/doctor/request/list/:specialtyType
router.get("/request/list/:specialtyType", auth, async (req, res) => {
  try {

    console.log("req.params.specialtyType",req.params.specialtyType)

    const requests = await Request.find({ specialtyType: req.params.specialtyType, status: 0 })
      .populate("patientId", "name surname")
      .sort({ createdAt: -1 });

      console.log("requests",requests);

    res.json({ requests });
  } catch (e) {
    console.error(e);
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/doctor/request/confirm
router.post("/request/confirm", auth, async (req, res) => {
  try {
    const {
      requestId,
      patientId,
      id,
      specialtyType, 
      startDate, 
      startTime } = req.body;

    const examination = new Examination({ 
      patientId,
      doctorId: id,
      specialtyType, 
      startDate, 
      startTime,
      status: 0
     });

    await examination.save();

    await Request.update(
      {_id: requestId}, 
      {$set: {status: 1}}, 
      (e) => console.log(e));

    res.status(201).json({ message: "Examination created" });
  } catch (e) {
    console.error(e);
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/doctor/examination/list/:doctorId
router.get("/examination/list/:doctorId", auth, async (req, res) => {
  try {

    const examinations = await Examination.find({ doctorId: req.params.doctorId })
      .populate("patientId", "name surname")
      .sort({ createdAt: -1 });

    res.json({ examinations });
  } catch (e) {
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});


// api/doctor/examination/review
router.post("/examination/review", auth, async (req, res) => {
  try {
    const { id, review } = req.body;

      await Examination.update(
        {_id: id}, 
        {$set: { review }}, 
        (e) => console.log(e));

    res.status(200).json({ message: "Review is sent" });
  } catch (e) {
    console.error(e);
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

module.exports = router;

const { Router } = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");

const Patient = require("../models/Patient");
const Doctor = require("../models/Doctor");
const Request = require("../models/Request");
const Examination = require("../models/Examination");

const { validateEmail, validateName, validatePassword, validateSurname } = require("../utils/validatiors");

const auth = require("../middleware/auth.middleware.js");

const router = Router();

// api/patient/register
router.post("/register", async (req, res) => {
  try {
    const { name, surname, email, password, repeatPassword } = req.body;

    const patientToRegister = await Patient.findOne({ email });
    if (patientToRegister) {
      console.error("ERROR: USER ALREADY EXIST");
      return res.status(400).json({ message: "User already exist" });
    }

    if (password !== repeatPassword) {
      console.error("ERROR: PASSWORDS AREN'T EQUAL");
      return res.status(400).json({ message: "Passwords aren't equal" });
    }

    let validatedName = validateName(name);
    if(!validatedName.OK){
      return res.status(400).json({ message: validatedName.message });
    }

    let validatedSurname = validateSurname(surname);
    if(!validatedSurname.OK){
      return res.status(400).json({ message: validatedSurname.message });
    }

    let validatedEmail = validateEmail(email);
    if(!validatedEmail.OK){
      return res.status(400).json({ message: validatedEmail.message });
    }

    let validatedPassword = validatePassword(password);
    if(!validatedPassword.OK){
      return res.status(400).json({ message: validatedPassword.message });
    }

    console.log("here")

    const hashedPassword = await bcrypt.hash(password, 15);
    const user = new Patient({ name, surname, email, password: hashedPassword });

    await user.save();

    const createdUser = await Patient.findOne({ name, surname, email });

    console.log("patient register createdUser", createdUser);

    const token = jwt.sign(
      {
        userId: user.id,
      },
      config.get("jwtSecret"),
      { expiresIn: "30d" }
    );

    res.json({ token, userId: createdUser.id, name, surname, email });
  } catch (e) {
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/patient/login
router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await Patient.findOne({ email });

    let isMatch;
    if (user) {
      isMatch = await bcrypt.compare(password, user.password);
    }

    if (!user || !isMatch) {
      return res.status(400).json({ message: "Incorrect data" });
    }

    const token = jwt.sign(
      {
        userId: user.id,
      },
      config.get("jwtSecret"),
      { expiresIn: "30d" }
    );

    res.json({ 
      token,
      userId: user.id,
      name: user.name,
      surname: user.surname,
      email: user.email
    });
  } catch (e) {
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/patient/profile/update
router.post("/profile/update", auth, async (req, res) => {
  try {
    const { id, name, surname, email, password } = req.body;

    let fieldsToUpdate = {
      name, surname, email, password
    }

    if(!name){
      delete fieldsToUpdate.name;
    }

    if(!surname){
      delete fieldsToUpdate.surname;
    }

    if(!email){
      delete fieldsToUpdate.email;
    }

    if(!password){
      delete fieldsToUpdate.password;
    }

    if(email){
      const emailToUpdate = await Doctor.findOne({ email });
      if (emailToUpdate) {
        console.error("ERROR: USER ALREADY EXIST");
        return res.status(400).json({ message: "Email is in use" });
      }
      let validatedEmail = validateEmail(email);
      if(!validatedEmail.OK){
        return res.status(400).json({ message: validatedEmail.message });
      }
    }

    if(name){
      let validatedName = validateName(name);
      if(!validatedName.OK){
        return res.status(400).json({ message: validatedName.message });
      }
    }

    if(surname){
      let validatedSurname = validateSurname(surname);
      if(!validatedSurname.OK){
        return res.status(400).json({ message: validatedSurname.message });
      }
    }

    let hashedPassword;

    if(password){
      let validatedPassword = validatePassword(password);
      if(!validatedPassword.OK){
        return res.status(400).json({ message: validatedPassword.message });
      }
      hashedPassword = await bcrypt.hash(password, 15);
    }

    await Patient.update(
      {_id: id}, 
      {$set: { ...fieldsToUpdate, password: hashedPassword }}, 
      (e) => console.log(e));

    const updatedUser = await Patient.findOne({ _id: id });

    res.status(201).json(updatedUser);
  } catch (e) {
    console.error(e);
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

// api/patient/examination/:patientId
router.get("/examination/:patientId", auth, async (req, res) => {
  try {

    const examinations = await Examination.find({ patientId: req.params.patientId })
      .populate("doctorId", "name surname")
      .sort({ createdAt: -1 });

    res.json({ examinations });
  } catch (e) {
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});


// api/patient/examination/book
router.post("/examination/book", auth, async (req, res) => {
  try {
    const {
      patientId,
      specialtyType, 
      appropriateDateStart, 
      appropriateTimeStart, 
      appropriateDateEnd, 
      appropriateTimeEnd } = req.body;

    const request = new Request({ 
      patientId,
      specialtyType, 
      appropriateDateStart, 
      appropriateTimeStart, 
      appropriateDateEnd, 
      appropriateTimeEnd,
      status: 0
     });

    await request.save();

    res.status(201).json({ message: "Request created" });
  } catch (e) {
    console.error(e);
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});


// api/patient/examination/result/:examinationId
router.get("/examination/result/:examinationId", auth, async (req, res) => {
  try {

    const examination = await Examination.findOne({ _id: req.params.examinationId })
      .sort({ createdAt: -1 });

    res.json({ result: examination.review });
  } catch (e) {
    res.status(500).json({ message: "Something is wrong, try again" });
  }
});

module.exports = router;

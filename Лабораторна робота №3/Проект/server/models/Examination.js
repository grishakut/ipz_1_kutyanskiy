const { Schema, model, Types } = require("mongoose");

const schema = new Schema(
  {
    specialtyType: { type: Number, required: "true" },
    status: { type: Number, required: "true" },
    createdAt: { type: Date, default: Date.now, required: "true" },
    startDate: { type: String, default: null, required: "true" },
    startTime: { type: String, default: "09:00 AM", required: "true" },
    review: { type: String, default: "" },
    doctorId: { type: Types.ObjectId, ref: "Doctor", default: null, required: "true" },
    patientId: { type: Types.ObjectId, ref: "Patient", default: null, required: "true" },
  },
  {
    timestamps: true,
  }
);

module.exports = model("Examination", schema);

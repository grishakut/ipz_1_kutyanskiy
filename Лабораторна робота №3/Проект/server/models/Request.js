const { Schema, model, Types } = require("mongoose");

const schema = new Schema(
  {
    specialtyType: { type: Number, required: "true" },
    createdAt: { type: Date, default: Date.now, required: "true" },
    appropriateDateStart: { type: String, default: null, required: "true" },
    appropriateTimeStart: { type: String, default: "09:00 AM", required: "true" },
    appropriateDateEnd: { type: String, default: null, required: "true" },
    appropriateTimeEnd: { type: String, default: "08:00 PM", required: "true" },
    status: { type: Number, required: "true" },
    patientId: { type: Types.ObjectId, ref: "Patient", default: null, required: "true" },
  },
  {
    timestamps: true,
  }
);

module.exports = model("Request", schema);

const validatePassword = (password) => {
    if (password.length < 4) {
      return { OK: false, message: "Password's length should be from 4 symbols" };
    }
    if (password.length >= 31) {
      return { OK: false, message: "Password's length should be less than 30 symbols" };
    }
    return {OK: true};
}

const validateName = (name) => {
    if (name.length < 2) {
      return { OK: false, message: "Name's length should be from 2 symbols" };
    }
    if (name.length >= 16) {
      return { OK: false, message: "Name's length should be less than 16 symbols" };
    }
    return {OK: true};
}

const validateSurname = (surname) => {
    if (surname.length < 2) {
      return { OK: false, message: "Surname's length should be from 2 symbols" };
    }
    if (surname.length >= 40) {
      return { OK: false, message: "Surname's length should be less than 16 symbols" };
    }
    return {OK: true};
}

const validateEmail = (email) => {
    if (!email.includes("@") || !email.includes(".")) {
      return { OK: false, message: "Incorrect email" };
    }
    if (email.length < 6) {
      return { OK: false, message: "Email's length should be from 2 symbols" };
    }
    if (email.length >= 30) {
      return { OK: false, message: "Email's length should be less than 30 symbols" };
    }
    return {OK: true};
}

module.exports = {validatePassword, validateName, validateSurname, validateEmail}
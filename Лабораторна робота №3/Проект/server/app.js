const express = require("express");
const localtunnel = require("localtunnel");
const config = require("config");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

const doctorRouter = require("./routes/doctor.routes");
const patientRouter = require("./routes/patient.routes");

const app = express();

app.use(function(req, res, next) {
  if (req.headers.origin) {
      res.header('Access-Control-Allow-Origin', '*')
      res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization')
      res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE')
      if (req.method === 'OPTIONS') return res.send(200)
  }
  next()
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api/doctor", doctorRouter);
app.use("/api/patient", patientRouter);

const PORT = config.get("port") || 5000;

async function start() {
  try {
    await mongoose.connect(config.get("mongoUri"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
   /*  app.listen(PORT, () =>
      console.log(`App has been started on port ${PORT}...`)
    ); */

    const server = app.listen(PORT, () => {
      console.log("Running at " + PORT);
    });
    
    const tunnel = localtunnel(
      PORT,
      { subdomain: "hospitaladministrationapi" },
      (err, tunnel) => {
        console.log("Server is working", tunnel);
      }
    );
    
    tunnel.on("close", function () {
      tunnel.close();
      console.log("Closing tunnel");
    });
    
  } catch (e) {
    console.error("Server Error", e.message);
    process.exit(1);
  }
}
start();

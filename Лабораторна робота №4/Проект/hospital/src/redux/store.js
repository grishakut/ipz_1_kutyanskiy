import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import thunk from "redux-thunk";
import { authReducer } from "./reducers/authReducer";
import { patientReducer } from "./reducers/patientReducer";
import { doctorReducer } from "./reducers/doctorReducer";
import { commonReducer } from "./reducers/commonReducer";

let reducers = combineReducers({
  auth: authReducer,
  patient: patientReducer,
  doctor: doctorReducer,
  common: commonReducer
});

export const store = createStore(
  reducers,
  compose(
    applyMiddleware(thunk)
    /* window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() */
  )
);

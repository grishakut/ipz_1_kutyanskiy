import {
  AUTH_SET_USER_IS_DOCTOR,
  SET_FETCHING_IN_PROCESS,
  AUTH_SET_USER_AUTH,
  SET_SERVER_ERROR,
  AUTH_LOGOUT,
  PATIENT_EXAMINATION_SET_EXAMINATIONS,
  DOCTOR_EXAMINATION_SET_REQUESTS,
  DOCTOR_EXAMINATION_SET_EXAMINATIONS
} from "../types/types";

import { requestAPI } from "../../api/requestApi";

export const setUserIsDoctor = (isDoctor) => ({
  type: AUTH_SET_USER_IS_DOCTOR,
  isDoctor,
});

export const setFetchingInProcess = (fetchingInProcess) => ({
  type: SET_FETCHING_IN_PROCESS,
  fetchingInProcess,
});

export const setUserAuth = (userData) => ({
  type: AUTH_SET_USER_AUTH,
  userData,
});

export const authUser = (role, type, formData) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const data = await requestAPI.request(`/api/` + role + `/${type}`, "POST", {
      ...formData,
    });
    const authData = {
      email: data.email,
      id: data.userId,
      name: data.name,
      surname: data.surname,
      isAuthenticated: true,
      token: data.token,
    }

    console.log("data", data);
    console.log("authData", authData);

    if(data.specialtyType >= 0){
      authData.specialtyType = data.specialtyType
    }else{
      authData.specialtyType = null;
    }

    console.log("authData", authData);

    dispatch(
      setUserAuth({ ...authData})
    );

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};

export const logout = () => ({
  type: AUTH_LOGOUT
});

export const setServerError = (serverError) => ({
  type: SET_SERVER_ERROR,
  serverError,
});

export const sendExaminationRequest = (formData, token) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const data = await requestAPI.request(`/api/patient/examination/book`, "POST", {
      ...formData,
    },
    {
      Authorization: `Bearer ${token}`,
    });

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};

export const updateProfile = (formData, role, token) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const data = await requestAPI.request("/api/" + role + "/profile/update", "POST", {
      ...formData,
    },
    {
      Authorization: `Bearer ${token}`,
    });

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};

export const setExaminations = (examinations) => ({
  type: PATIENT_EXAMINATION_SET_EXAMINATIONS,
  examinations,
});

export const getListOfPatientsExaminations = (token, patientId) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const examinations = await requestAPI.request(
      `/api/patient/examination/${patientId}`,
      "GET",
      null,
      {
        Authorization: `Bearer ${token}`,
      }
    );

    dispatch(setExaminations(examinations.examinations));

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};

export const setDoctorExaminationRequests = (requests) => ({
  type: DOCTOR_EXAMINATION_SET_REQUESTS,
  requests,
});

export const getListOfExaminationRequests = (token, specialtyType) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const requests = await requestAPI.request(
      `/api/doctor/request/list/${specialtyType}`,
      "GET",
      null,
      {
        Authorization: `Bearer ${token}`,
      }
    );

    console.log("requests.requests",requests.requests);

    dispatch(setDoctorExaminationRequests(requests.requests));

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};


export const setDoctorExaminations = (examinations) => ({
  type: DOCTOR_EXAMINATION_SET_EXAMINATIONS,
  examinations,
});

export const getListOfDoctorsExaminations = (token, doctorId) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const examinations = await requestAPI.request(
      `/api/doctor/examination/list/${doctorId}`,
      "GET",
      null,
      {
        Authorization: `Bearer ${token}`,
      }
    );

    dispatch(setDoctorExaminations(examinations.examinations));

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};

export const confirmExamination = (formData, token) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const data = await requestAPI.request(`/api/doctor/request/confirm`, "POST", {
      ...formData,
    },
    {
      Authorization: `Bearer ${token}`,
    });

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};


export const sendExaminationReview = (formData, token) => async (dispatch) => {
  try {
    dispatch(setFetchingInProcess(true));
    const data = await requestAPI.request(`/api/doctor/examination/review`, "POST", {
      ...formData,
    },
    {
      Authorization: `Bearer ${token}`,
    });

    dispatch(setFetchingInProcess(false));
    dispatch(setServerError(null));
  } catch (e) {
    dispatch(setServerError(e.message));
    setTimeout(() => dispatch(setServerError(null)), 2000);
    console.error(e.message);
    dispatch(setFetchingInProcess(false));
  }
};


import { AUTH_SET_USER_IS_DOCTOR, AUTH_SET_USER_AUTH, AUTH_LOGOUT } from "../types/types";

const initialState = {
  isDoctor: null,
  id: null,
  token: null,
  email: null,
  name: null,
  surname: null,
  specialtyType: null,
  isAuthenticated: false
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_SET_USER_IS_DOCTOR:
      return Object.assign({}, state, {
        isDoctor: action.isDoctor,
      });
    case AUTH_SET_USER_AUTH:
      return Object.assign({}, state, {
        email: action.userData.email,
        id: action.userData.id,
        name: action.userData.name,
        surname: action.userData.surname,
        specialtyType: action.userData.specialtyType,
        isAuthenticated: true,
        token: action.userData.token,
      });
    case AUTH_LOGOUT:
      return Object.assign({}, state, {
        id: null,
        token: null,
        email: null,
        name: null,
        surname: null,
        specialtyType: null,
        isAuthenticated: false
      });
    default:
      return state;
  }
};

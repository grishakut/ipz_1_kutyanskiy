import {
  DOCTOR_EXAMINATION_SET_REQUESTS,
  DOCTOR_EXAMINATION_SET_EXAMINATIONS,
  EXAMINATION_SET_EXAMINATIONS_COUNT,
} from "../types/types";

const initialState = {
  examinations: [],
  examinationsCount: null,
  requests: []
};

export const doctorReducer = (state = initialState, action) => {
  switch (action.type) {
    case DOCTOR_EXAMINATION_SET_REQUESTS:
      return Object.assign({}, state, {
        requests: action.requests,
      });
    case DOCTOR_EXAMINATION_SET_EXAMINATIONS:
      return Object.assign({}, state, {
        examinations: action.examinations,
      });
    case EXAMINATION_SET_EXAMINATIONS_COUNT:
      return Object.assign({}, state, {
        examinationsCount: action.examinationsCount,
      });
    default:
      return state;
  }
};

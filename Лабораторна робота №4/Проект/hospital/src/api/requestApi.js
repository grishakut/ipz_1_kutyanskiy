export const requestAPI = {
  request: async (url, method = "GET", body = null, headers = {}) => {
    try {
      if (body) {
        body = JSON.stringify(body);
        headers["Content-Type"] = "application/json";
      }

      const apiUrl = "http://localhost:5000" + url;

      //const apiUrl = "http://localhost:5000/api/doctor/login";
      console.log("apiUrl",apiUrl);

      const response = await fetch(apiUrl, { method, body, headers });
      /* const response = await fetch(apiUrl, { method: "POST", body: {
        "email": "mike292jackson@gmail.com", 
        "password": "1111"
    }, headers: {
      "Content-Type": "application/json"
    } }); */
      const data = await response.json();

      if (!response.ok) {
        throw new Error(data.message || "Something is wrong");
      }
      return data;
    } catch (e) {
      console.log(e.message);
      throw e;
    }
  },
};

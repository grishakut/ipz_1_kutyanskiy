import React from "react";
import { HashRouter as Router, Route } from "react-router-dom";

import GeneralLayout from "./components/Common/GeneralLayout";

import Login from "./components/Common/Login/Login";
import Registration from "./components/Common/Registration/Registration";
import RoleSelector from "./components/RoleSelector";

import Doctor from "./components/Doctor/Doctor";
import Patient from "./components/Patient/Patient";

import "./App.css";

import "fontsource-roboto";

const App = () => {
  return (
    <GeneralLayout>
      <Router>
        <div>
          <main>
            <Route exact path="/" component={RoleSelector} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Registration} />

            <Route path="/doctor" component={Doctor} />
            <Route path="/patient" component={Patient} />
          </main>
        </div>
      </Router>
    </GeneralLayout>
  );
};

export default App;

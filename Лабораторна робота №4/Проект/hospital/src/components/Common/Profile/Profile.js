import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";

import {updateProfile} from "../../../redux/actions/actions";

import { specialties } from "../Specialties";

import classes from "./Profile.module.css";

const Profile = ({ isDoctor, id, email, name, surname, specialtyType, updateProfile, token }) => {
  const [profileSpecialty, setSpecialty] = React.useState(specialties[specialtyType]);
  const [profileName, setName] = React.useState(name);
  const [profileSurname, setSurname] = React.useState(surname);
  const [profileEmail, setEmailText] = React.useState(email);

  const [passwordText, setPasswordText] = React.useState("");
  const [repeatPasswordText, setRepeatPasswordText] = React.useState("");

  console.log("profileSpecialty",profileSpecialty,
  "profileName",profileName,
  "profileSurname",profileSurname,
  "profileEmail",profileEmail )

  const onNameTextChange = (e) => {
    setName(e.target.value);
  };

  const onSurnameTextChange = (e) => {
    setSurname(e.target.value);
  };

  const onEmailTextChange = (e) => {
    setEmailText(e.target.value);
  };

  const onPasswordTextChange = (e) => {
    setPasswordText(e.target.value);
  };

  const onRepeatPasswordTextChange = (e) => {
    setRepeatPasswordText(e.target.value);
  };

  const onSpecialtyChange = (e) => {
    setSpecialty(e.target.value);
  };

  const onProfileChangeSubmit = async (field, value) => {
    let role = isDoctor? "doctor": "patient";

    console.log("field",field);
    console.log("value",value);

    await updateProfile({
      id,
      [field]: value
    }, role, token);

    console.log("hello", profileEmail, passwordText);
  };

  return (
    <div className={classes.profileContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Profile
        </Typography>
      </Box>
      <Box mt={2}>
        <Paper className={classes.dataContainer} elevation={10}>
          <Box display="flex" alignItems="center">
            <TextField
              fullWidth
              id="name-register-input"
              label="New name"
              type="text"
              variant="outlined"
              margin="dense"
              value={profileName}
              onChange={onNameTextChange}
            />
            <Box ml={1}>
              <Button variant="outlined" color="primary" onClick={() => onProfileChangeSubmit("name", profileName)}>
                Change
              </Button>
            </Box>
          </Box>
          <Box mt={1} display="flex" alignItems="center">
            <TextField
              fullWidth
              id="surname-register-input"
              label="New surname"
              type="text"
              variant="outlined"
              margin="dense"
              value={profileSurname}
              onChange={onSurnameTextChange}
            />
            <Box ml={1}>
              <Button variant="outlined" color="primary" onClick={() => onProfileChangeSubmit("surname", profileSurname)}>
                Change
              </Button>
            </Box>
          </Box>
          <Box mt={1} display="flex" alignItems="center">
            <TextField
              fullWidth
              id="email-register-input"
              label="New email"
              type="email"
              variant="outlined"
              margin="dense"
              value={profileEmail}
              onChange={onEmailTextChange}
            />
            <Box ml={1}>
              <Button variant="outlined" color="primary" onClick={() => onProfileChangeSubmit("email", profileEmail)}>
                Change
              </Button>
            </Box>
          </Box>
          <Box mt={1} display="flex" alignItems="center">
            <TextField
              fullWidth
              id="password-register-input"
              label="New password"
              type="text"
              variant="outlined"
              margin="dense"
              value={passwordText}
              onChange={onPasswordTextChange}
            />
            <Box ml={1}>
              <Button variant="outlined" color="primary" onClick={() => onProfileChangeSubmit("password", passwordText)}>
                Change
              </Button>
            </Box>
          </Box>
          {isDoctor && (
            <Box mt={1} display="flex" alignItems="center">
              <TextField
                id="specialty-register-input"
                select
                fullWidth
                label="Specialty"
                value={profileSpecialty}
                onChange={onSpecialtyChange}
              >
                {specialties.map((specialty) => (
                  <MenuItem key={specialty} value={specialty}>
                    {specialty}
                  </MenuItem>
                ))}
              </TextField>
              <Box ml={1}>
                <Button variant="outlined" color="primary" onClick={() => onProfileChangeSubmit("specialtyType", specialties.indexOf(profileSpecialty))}>
                  Change
                </Button>
              </Box>
            </Box>
          )}
        </Paper>
      </Box>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isDoctor: state.auth.isDoctor,
    token: state.auth.token,
    id: state.auth.id,
    email: state.auth.email,
    name: state.auth.name,
    surname: state.auth.surname,
    specialtyType: state.auth.specialtyType,
  };
};

export default connect(mapStateToProps, {updateProfile})(Profile);

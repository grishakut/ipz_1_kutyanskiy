import React from "react";
import { connect } from "react-redux";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    background: "linear-gradient(45deg, #28738f 30%, #2fba64 90%)",
    minHeight: "100vh",
    height: "100%",
  },
});

const GeneralLayout = ({ children, serverError }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <Grid
        container
        justify="center"
        alignItems="flex-start"
        className={classes.root}
      >
        {serverError && 
            <div style={{
              position: "fixed",
              backgroundColor: "red", 
              color: "white", 
              padding: 20, 
              margin: "120px 0 0 0", 
              opacity: 0.8, 
              zIndex: 999}}><b>{serverError}</b></div>}
        {children}
      </Grid>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    serverError: state.common.serverError
  };
};

export default connect(mapStateToProps, null)(GeneralLayout);
import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import moment from "moment";
import 'moment-timezone';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Grid from "@material-ui/core/Grid";

import {confirmExamination} from "../../../../../redux/actions/actions";

import { specialties } from "../../../../Common/Specialties";

import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";

import classes from "./RequestConfirmation.module.css";

const BookButton = withStyles((theme) => ({
  root: {
    color: "#28738f",
    "&:hover": {
      color: " #2fba64",
      backgroundColor: "transparent",
    },
  },
}))(Button);

const RequestConfirmation = (props) => {
  const [selectedDate, setSelectedDate] = React.useState(
    new Date("2021-01-15T16:00:00")
  );

  const onDateChange = (date) => {
    setSelectedDate(date);
  };

  const onConfirmExamination = async () => {
    let examinationDate = moment(+selectedDate).format("DD.MM.YYYYTHH:mm").split("T");

    const examinationData = {
      patientId: props.location.state.patient._id,
      id: props.id,
      specialtyType: props.location.state.specialtyType, 
      startDate: examinationDate[0], 
      startTime: examinationDate[1],
      requestId: props.location.state.requestId
    }

    console.log("examinationData", examinationData);

    await props.confirmExamination(examinationData, props.token);

    props.history.push(`/doctor/examinations`);
  }

  return (
    <div className={classes.requestConfirmationContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Request confirmation
        </Typography>
      </Box>
      <Box mt={2}>
        <Typography variant="subtitle1">Request date: {moment.utc(props.location.state.createdAt).local().format("HH:mm DD.MM.YYYY")}</Typography>
        <Typography variant="subtitle1">Patient:&nbsp;
        {props.location.state.patient.name + " " + props.location.state.patient.surname}</Typography>
        <Typography variant="subtitle1">
          Availability: {props.location.state.appropriateDateStart}
          &nbsp;-&nbsp;
          {props.location.state.appropriateDateEnd},
          &nbsp;{props.location.state.appropriateTimeStart}
          &nbsp;-&nbsp;
          {props.location.state.appropriateTimeEnd}
        </Typography>
      </Box>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container>
          <Box style={{ width: "100%" }}>
            <Typography variant="h6">Please, set examination date</Typography>
          </Box>

          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="dd/MM/yyyy"
            margin="normal"
            id="pick-date"
            label="Pick date"
            value={selectedDate}
            onChange={onDateChange}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </Grid>
        <Grid container>

          <KeyboardTimePicker
            margin="normal"
            id="pick-time"
            label="Pick time"
            value={selectedDate}
            onChange={onDateChange}
            KeyboardButtonProps={{
              "aria-label": "change time",
            }}
          />
        </Grid>
      </MuiPickersUtilsProvider>
      <Box pt={2}>
        <BookButton onClick={onConfirmExamination} variant="outlined">Confirm</BookButton>
      </Box>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    id: state.auth.id,
  };
};

export default withRouter(connect(mapStateToProps, {confirmExamination})(RequestConfirmation));


import React from "react";
import { connect } from "react-redux";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";


import {getListOfExaminationRequests} from "../../../../redux/actions/actions";

import RequestItem from "./RequestItem/RequestItem";

import classes from "./ExaminationRequests.module.css";

const ExaminationRequests = ({id, token, specialtyType, requests, getListOfExaminationRequests}) => {
  React.useEffect(() => {
    console.log("examination patient list useEffect");
    const fetchExaminationRequests = async () => {
      await getListOfExaminationRequests(token, specialtyType);
    }

    fetchExaminationRequests();
  }, [])
  return (
    <div className={classes.requestsContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Examination requests
        </Typography>
      </Box>
      {requests.length > 0 && (
        <Box mt={1} display="flex" justifyContent="space-between" padding={1}>
          <Typography variant="subtitle1">Date</Typography>
          <Typography variant="subtitle1">Patient's name</Typography>
          <Typography variant="subtitle1">Appropriate date</Typography>
        </Box>
      )}
      <Box className={classes.requestItemsContainer}>
      {requests.length === 0? 
        (<Typography variant="subtitle1" gutterBottom>You have no requests</Typography>): 
        requests.map((request) => (
          <RequestItem 
            specialtyType={request.specialtyType}
            patient={request.patientId}
            doctorId={request.id}
            appropriateDateStart={request.appropriateDateStart}
            appropriateTimeStart={request.appropriateTimeStart}
            appropriateDateEnd={request.appropriateDateEnd}
            appropriateTimeEnd={request.appropriateTimeEnd}
            status={request.status}
            createdAt={request.createdAt}
            requestId={request._id}
            key={request._id}
          />
        ))}
      </Box>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    specialtyType: state.auth.specialtyType,
    id: state.auth.id,
    requests: state.doctor.requests
  };
};

export default connect(mapStateToProps, {getListOfExaminationRequests})(ExaminationRequests);

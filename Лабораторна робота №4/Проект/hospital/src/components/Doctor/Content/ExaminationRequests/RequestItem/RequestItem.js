import React from "react";
import { Link } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import moment from "moment";

import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";

import classes from "./RequestItem.module.css";

const RequestItem = (props) => {
  return (
    <Paper className={classes.itemContainer}>
      <Link to={{
        pathname: '/doctor/request-confirmation',
        state: {...props}
      }}>
        <Box mt={2} display="flex" justifyContent="space-around" padding={1}>
          <Typography variant="subtitle2">{moment.utc(props.createdAt).local().format("HH:mm DD.MM.YYYY")}</Typography>
          <Typography variant="subtitle2">{props.patient.name + " "+ props.patient.surname}</Typography>&nbsp;
          <Typography variant="subtitle2">
            {props.appropriateDateStart}&nbsp;-&nbsp;{props.appropriateDateEnd},&nbsp;
            {props.appropriateTimeStart}&nbsp;-&nbsp;{props.appropriateTimeEnd}
          </Typography>
        </Box>
      </Link>
    </Paper>
  );
};

export default RequestItem;

import React from "react";
import { Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import classes from "./ExaminationItem.module.css";

const ExaminationItem = ({specialtyType, token, patient, id, review, startDate, startTime}) => {
  return (
    <Paper className={classes.examItemContainer}>
      <Link to={{
        pathname: '/doctor/review',
        state: {
          id,
          token,
          review
        }
      }}>
        <Box mt={2} display="flex" justifyContent="space-between" padding={1}>
          <Typography variant="subtitle2">{startTime + " " + startDate}</Typography>
          <Typography variant="subtitle2">{patient.name + " " + patient.surname}</Typography>&nbsp;
        </Box>
      </Link>
    </Paper>
  );
};

export default ExaminationItem;

import React from "react";
import { connect } from "react-redux";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import {getListOfDoctorsExaminations} from "../../../../redux/actions/actions";

import ExaminationItem from "./ExaminationItem/ExaminationItem";

import classes from "./Examinations.module.css";

const Examinations = ({id, token, examinations, getListOfDoctorsExaminations}) => {

  React.useEffect(() => {
    console.log("examination doctors list useEffect");
    const fetchExaminations = async () => {
      await getListOfDoctorsExaminations(token, id);
    }

    fetchExaminations();
  }, [])

  return (
    <div className={classes.examinationsContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Examinations
        </Typography>
      </Box>

      {examinations.length > 0 && (
        <Box mt={1} display="flex" justifyContent="space-between" padding={1}>
          <Typography variant="subtitle1">Examination date</Typography>
          <Typography variant="subtitle1">Patient's name</Typography>&nbsp;
        </Box>
      )}
        <Box className={classes.examItemsContainer}>
      {examinations.length === 0? 
        (<Typography variant="subtitle1" gutterBottom>You have no examinations</Typography>): 
        examinations.map((examination) => (
          <ExaminationItem 
            specialtyType={examination.specialtyType}
            token={token}
            patient={examination.patientId}
            id={examination._id}
            review={examination.review}
            startDate={examination.startDate}
            startTime={examination.startTime}
            key={examination._id}
          />
        ))}
      </Box>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    specialtyType: state.auth.specialtyType,
    id: state.auth.id,
    examinations: state.doctor.examinations
  };
};

export default connect(mapStateToProps, {getListOfDoctorsExaminations})(Examinations);

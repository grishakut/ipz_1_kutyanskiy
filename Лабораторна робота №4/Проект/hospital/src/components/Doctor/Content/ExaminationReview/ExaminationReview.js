import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import {sendExaminationReview} from "../../../../redux/actions/actions";

import classes from "./ExaminationReview.module.css";

const SendButton = withStyles((theme) => ({
  root: {
    color: "#28738f",
    "&:hover": {
      color: " #2fba64",
      backgroundColor: "transparent",
    },
  },
}))(Button);

const ExaminationReview = (props) => {
  const [reviewText, setReviewText] = React.useState(props.location.state.review);
  const [reviewError, setReviewError] = React.useState(false);

  const onReviewTextChange = (e) => {
    setReviewText(e.target.value);
  };

  const onSendExamReview = async () => {
    await props.sendExaminationReview({
      id: props.location.state.id,
      review: reviewText
    }, props.location.state.token);

    props.history.push(`/doctor/examinations`);
  }

  console.log("props.location.state", props.location.state);

  return (
    <div className={classes.reviewContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Examination review
        </Typography>
      </Box>
      <Box>
        <TextField
          multiline={true}
          rows={10}
          required
          fullWidth
          autoFocus
          id="review-login-input"
          label="Review"
          type="text"
          variant="outlined"
          margin="dense"
          value={reviewText}
          error={reviewError}
          onChange={onReviewTextChange}
        />
      </Box>
      <Box pt={2}>
        <SendButton onClick={onSendExamReview} variant="outlined">Send examination review</SendButton>
      </Box>
    </div>
  );
};

export default withRouter(connect(null, {sendExaminationReview})(ExaminationReview));


import "date-fns";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import React from "react";
import Grid from "@material-ui/core/Grid";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import moment from "moment";

import {sendExaminationRequest} from "../../../../redux/actions/actions";

import classes from "./ExaminationBooking.module.css";

import { specialties } from "../../../Common/Specialties";

const BookButton = withStyles((theme) => ({
  root: {
    color: "#28738f",
    "&:hover": {
      color: " #2fba64",
      backgroundColor: "transparent",
    },
  },
}))(Button);

const ExaminationBooking = ({history, id, token, sendExaminationRequest}) => {
  const [selectedStartDate, setSelectedStartDate] = React.useState(
    new Date("2021-01-06T13:00:00")
  );
  const [selectedEndDate, setSelectedEndDate] = React.useState(
    new Date("2021-01-25T19:30:00")
  );

  const [specialty, setSpecialty] = React.useState("");

  const onSpecialtyChange = (e) => {
    setSpecialty(e.target.value);
  };

  const onStartDateChange = (date) => {
    setSelectedStartDate(date);
    console.log('date', selectedStartDate);
  };
  const onEndDateChange = (date) => {
    setSelectedEndDate(date);
    console.log('date', selectedEndDate);
  };

  const onSendRequest = async () => {
    let apprDateStart = moment(+selectedStartDate).format("DD.MM.YYYYTHH:mm").split("T");
    let apprDateEnd = moment(+selectedEndDate).format("DD.MM.YYYYTHH:mm").split("T");

    console.log("specialties",specialties);
    console.log("specialty",specialty);
    console.log("specialties.indexOf(specialty)",specialties.indexOf(specialty));

    const examinationData = {
      patientId: id,
      specialtyType: specialties.indexOf(specialty), 
      appropriateDateStart: apprDateStart[0], 
      appropriateTimeStart: apprDateStart[1],
      appropriateDateEnd: apprDateEnd[0], 
      appropriateTimeEnd: apprDateEnd[1],
    }

    console.log("examinationData", examinationData);

    await sendExaminationRequest(examinationData, token);

    history.push(`/patient/`);
  }

  return (
    <div className={classes.reviewContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Examination Booking
        </Typography>
      </Box>
      <Box>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <Grid container justify="space-around">

            <Box style={{ width: "100%" }}>
              <Typography variant="h6">
                Please, pick doctor's specialty
              </Typography>
            </Box>

              <TextField
                id="specialty-book-input"
                select
                fullWidth
                label="Specialty"
                value={specialty}
                onChange={onSpecialtyChange}
              >
                {specialties.map((specialty) => (
                  <MenuItem key={specialty} value={specialty}>
                    {specialty}
                  </MenuItem>
                ))}
              </TextField>

            <Box style={{ width: "100%", marginTop: 30}}>
              <Typography variant="h6">
                Please, pick date and time range when you are available {specialty}
              </Typography>
            </Box>

            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="dd/MM/yyyy"
              margin="normal"
              id="pick-start-date"
              label="Pick starting date"
              value={selectedStartDate}
              onChange={onStartDateChange}
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
            />
            <KeyboardDatePicker
              disableToolbar
              variant="inline"
              format="dd/MM/yyyy"
              margin="normal"
              id="pick-end-date"
              label="Pick ending date"
              value={selectedEndDate}
              onChange={onEndDateChange}
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
            />
          </Grid>
          <Grid container justify="space-around">
            <KeyboardTimePicker
              margin="normal"
              id="pick-start-time"
              label="Pick starting time"
              value={selectedStartDate}
              onChange={onStartDateChange}
              KeyboardButtonProps={{
                "aria-label": "change time",
              }}
            />
            <KeyboardTimePicker
              margin="normal"
              id="pick-end-time"
              label="Pick ending time"
              value={selectedEndDate}
              onChange={onEndDateChange}
              KeyboardButtonProps={{
                "aria-label": "change time",
              }}
            />
          </Grid>
        </MuiPickersUtilsProvider>
        <Box pt={2}>
          <BookButton onClick={onSendRequest} variant="outlined">Send booking request</BookButton>
        </Box>
      </Box>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    id: state.auth.id,
    examinations: state.patient.examinations
  };
};

export default withRouter(connect(mapStateToProps, {sendExaminationRequest})(ExaminationBooking));


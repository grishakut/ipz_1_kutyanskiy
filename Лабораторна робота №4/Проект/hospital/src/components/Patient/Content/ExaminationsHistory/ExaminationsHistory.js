import React from "react";
import { connect } from "react-redux";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import {getListOfPatientsExaminations} from "../../../../redux/actions/actions";

import HistoryItem from "./HistoryItem/HistoryItem";

import classes from "./ExaminationsHistory.module.css";

const ExaminationsHistory = ({token, id, examinations, getListOfPatientsExaminations}) => {

  React.useEffect(() => {
    console.log("examination patient list useEffect");
    getListOfPatientsExaminations(token, id);
  }, [])
  return (
    <div className={classes.requestsContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Examinations History
        </Typography>
      </Box>
      {examinations.length > 0 && (
        <Box mt={1} pl={3} pr={5} display="flex" justifyContent="space-between" padding={1}>
          <Typography variant="subtitle1">Examination date</Typography>
          <Typography variant="subtitle1">Specialty</Typography>
          <Typography variant="subtitle1">Doctor's name</Typography>
        </Box>
      )}
      <Box className={classes.requestItemsContainer}>
        {examinations.length === 0? 
        (<Typography variant="subtitle1" gutterBottom>You have no confirmed examinations</Typography>): 
        examinations.map((examination) => (
          <HistoryItem 
            specialtyType={examination.specialtyType}
            review={examination.review}
            doctor={examination.doctorId}
            startDate={examination.startDate}
            startTime={examination.startTime}
            key={examination._id}
          />
        ))}
      </Box>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    id: state.auth.id,
    examinations: state.patient.examinations
  };
};

export default connect(mapStateToProps, {getListOfPatientsExaminations})(ExaminationsHistory);

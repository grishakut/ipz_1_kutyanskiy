import React from "react";
import { Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import { specialties } from "../../../../Common/Specialties";

import classes from "./HistoryItem.module.css";

const HistoryItem = ({specialtyType, review, doctor, startDate, startTime}) => {
  return (
    <Paper className={classes.itemContainer}>
      <Link to={{
        pathname: '/patient/results',
        state: {
          review
        }
      }} >
        <Box mt={2} pl={3} pr={5} display="flex" justifyContent="space-between" padding={1}>
          <Typography variant="subtitle2">{startTime + " " + startDate}</Typography>
          <Typography variant="subtitle2">{specialties[specialtyType]}</Typography>
          <Typography variant="subtitle2">
          {doctor.name + " " + doctor.surname}
          </Typography>
        </Box>
      </Link>
    </Paper>
  );
};

export default HistoryItem;

import React from "react";
import { withRouter } from "react-router";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

import classes from "./ExaminationResults.module.css";

const ExaminationResults = (props) => {
  const [reviewText, setReviewText] = React.useState("");
  const [reviewError, setReviewError] = React.useState(false);

  const onReviewTextChange = (e) => {
    setReviewText(e.target.value);
  };
  return (
    <div className={classes.reviewContainer}>
      <Box padding={1}>
        <Typography variant="h6" gutterBottom>
          Examination results
        </Typography>
      </Box>
      <Box padding={1}>
        <Typography variant="subtitle1">
          {props.location.state.review || <span>Here is the result of your examination.</span>}
        </Typography>
      </Box>
    </div>
  );
};

export default withRouter(ExaminationResults);
